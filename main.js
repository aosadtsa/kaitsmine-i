window.onload = function() {
	lingid = document.getElementById("gallery").getElementsByTagName('a');

	for (var i=0;i<lingid.length;i++){
		lingid[i].onmouseover=function(){
			over(this); 
		};
	}

	document.getElementById("frame").onmouseout=function(){
		this.style.display="none";
	};
}

function over(el){
	pilt=el.getElementsByTagName('img')[0];
	
	raam=document.getElementById("frame");
	filler=document.getElementById("filler");
	link=raam.getElementsByTagName('a')[0];
	
	imgborder=2;
	fillerborder=10;

	text=pilt.alt;
	document.getElementById("title").innerHTML=text;

	link.href=el.href;
	filler.src=pilt.src;
	filler.height=pilt.height;
	filler.width=pilt.width;

	off=pilt.getBoundingClientRect();

	raam.style.top=(off.top-(fillerborder-imgborder))+"px";
	raam.style.left=(off.left-(fillerborder-imgborder))+"px";
	raam.style.width=filler.width+(2*fillerborder)+"px";
	raam.style.display="block";

}